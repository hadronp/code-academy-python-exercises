def censor(text,word):
	replacestring = '*' * len(word)
	textlist = text.split(word)
	return replacestring.join(textlist)


def main():
	print censor("the quick brown fox jumps over the lazy dog", "the")


if __name__=='__main__':
	main()