def median(alist):
    n = sorted(alist)
    L = len(n)
    e = L/2

    if L%2 != 0:
     s = n[e]
     return s    
    else:
        j = n[e] + n[e-1]
        r = j/2.0
        return r
