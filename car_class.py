class Car(object):
	condition="new"

	def __init__(self,model,color,mpg):
		self.model=model
		self.color=color
		self.mpg=mpg

	def display_car(self):
		return "This is a {0} {1} with {2} MPG".format(self.color,self.model,self.mpg)

		# what works on code academy though
		#return "This is a %s %s with %d MPG." % (self.color, self.model, self.mpg)

	def drive_car(self):
		self.condition= "used"

class ElectricCar(Car):
	def __init__(self, model,color,mpg,battery_type):
		self.model=model
		self.color=color
		self.mpg=mpg
		self.battery_type=battery_type

	def drive_car(self):
		self.condition= "like new"


electric_car=ElectricCar("Ford Fiesta","red", 88, "molten salt")
print electric_car.display_car()
print electric_car.condition
electric_car.drive_car()
print electric_car.condition


electric_car=Car("DeLorean", "silver", 88)
print electric_car.condition
electric_car.drive_car()
print electric_car.condition