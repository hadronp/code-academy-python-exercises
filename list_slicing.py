my_list = range(1, 11) # List of numbers 1 - 10

# Add your code below
print my_list[::2]

backwards=my_list[::-1]
print backwards

to_one_hundred = range(101)
# Add your code below!
backwards_by_tens=to_one_hundred[::-10]
print backwards_by_tens

to_21=range(1,22)
odds=to_21[::2]
middle_third=to_21[7:14:1]

garbled = "!XeXgXaXsXsXeXmX XtXeXrXcXeXsX XeXhXtX XmXaX XI"
message= garbled[::-2]
print message

