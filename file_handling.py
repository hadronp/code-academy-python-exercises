# reading file
my_list = [i**2 for i in range(1,11)]
# Generates a list of squares of the numbers 1 - 10

f = open("output.txt", "w")

for item in my_list:
    f.write(str(item) + "\n")

f.close()

#ope file with r/w access
my_file=open("output.txt","r+")

#open and write file operations
my_list = [i**2 for i in range(1,11)]
my_file = open("output.txt", "r+")

for ml in my_list:
    my_file.write(str(ml)+ "\n")

my_file.close()

#open and read
my_file=open("output.txt","r")
print my_file.read()
my_file.close()

#open and read file line by line
my_file=open("text.txt","r")
print my_file.readline()
print my_file.readline()
print my_file.readline()
my_file.close()

#
# Open the file for reading
# The importance of closing files to flush data in buffer
read_file = open("text.txt", "r")

# Use a second file handler to open the file for writing
write_file = open("text.txt", "w")
# Write to the file
write_file.write("Not closing files is VERY BAD.")

write_file.close()

# Try to read from the file
print read_file.read()
read_file.close()


#
# using the with key word and as (alias)
#
with open("text.txt", "w") as my_file:
    my_file.write("Success!")   
print my_file.closed # True

#
# using the file obbject property .closed
with open("text.txt", "w") as my_file:
    my_file.write("the quick brown troll")   

if not my_file.closed:
    my_file.close()
print my_file.closed # True