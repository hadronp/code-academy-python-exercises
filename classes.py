class Fruit(object):
    """A class that makes various tasty fruits."""
    def __init__(self, name, color, flavor, poisonous):
        self.name = name
        self.color = color
        self.flavor = flavor
        self.poisonous = poisonous

    def description(self):
        print "I'm a %s %s and I taste %s." % (self.color, self.name, self.flavor)

    def is_edible(self):
        if not self.poisonous:
            print "Yep! I'm edible."
        else:
            print "Don't eat me! I am super poisonous."



lemon = Fruit("lemon", "yellow", "sour", False)
print lemon.description()
print lemon.is_edible()


class Animal(object):
    """Makes cute animals."""
    # For initializing our instance objects
    is_alive = True
    health="good"
    
    def __init__(self, name, age, is_hungry):
        self.name = name
        self.age = age
        self.is_hungry=is_hungry

    def description(self):
        print self.name
        print self.age

hippo=Animal("Max",10)
print hippo.description

zebra = Animal("Jeffrey", 2, True)
giraffe = Animal("Bruce", 1, False)
panda = Animal("Chad", 7, True)

print zebra.name, zebra.age, zebra.is_hungry
print giraffe.name, giraffe.age, giraffe.is_hungry
print panda.name, panda.age, panda.is_hungry

zebra=Animal("Jeffrey")
print zebra.name





