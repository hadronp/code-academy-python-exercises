def check_bit4(input):
	mask=0b1000
	
	if input & mask  > 0:
		return "on"
	else:
		return "off"
	
	#or simply
	#return 'on' if 0b1000 & input > 0 else 'off'

print check_bit4(1)
print check_bit4(2)
print check_bit4(4)
print check_bit4(8)
print check_bit4(9)

#turn bits on with a | mask
a = 0b10111011
mask=0b00000100
print bin(a | mask)

#flipping bits with a ^ mask
a = 0b11101110
mask=0b11111111
print bin(a ^ mask)

# custom mask via bit shifting to left or right
def flip_bit(number,n):
	mask= 0b1 << n-1

	return bin(number ^ mask)
