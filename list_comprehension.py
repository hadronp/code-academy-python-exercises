doubles_by_3 = [x*2 for x in range(1,6) if (x*2) % 3 == 0]

# Complete the following line. Use the line above for help.
even_squares = [x**2 for x in range(1,11) if (x**2) %2 ==0]

cubes_by_four= [x**3 for x in range(1,11) if (x**3) %4 ==0]
#cubes_by_four = [x ** 3 for x in range(1,11) if not x ** 3 % 4]

threes_and_fives=[x for x in range(1,16) if x%5==0 or x%3==0]

print doubles_by_3
print even_squares
print cubes_by_four
print threes_and_fives

